import 'package:flutter/material.dart';
import 'package:flutter_redux_starter/ui/app/app_drawer_vm.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HomeScreen extends StatelessWidget {
  static final String route = '/home';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate("common.ui.home.title")),
      ),
      drawer: AppDrawerBuilder(),
    );
  }
}
