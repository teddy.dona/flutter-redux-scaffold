import 'package:flutter_translate/flutter_translate.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_translate/localization_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux_starter/redux/app/app_middleware.dart';
import 'package:flutter_redux_starter/redux/app/app_state.dart';
import 'package:flutter_redux_starter/redux/app/app_reducer.dart';
import 'package:flutter_redux_starter/redux/auth/auth_middleware.dart';
import 'package:flutter_redux_starter/ui/app/init.dart';
import 'package:flutter_redux_starter/ui/auth/login_vm.dart';
import 'package:flutter_redux_starter/ui/home/home_screen.dart';
// STARTER: import - do not remove comment

main() async {
  var delegate = await LocalizationDelegate.create(
      fallbackLocale: 'fr', supportedLocales: ['en', 'fr']);
  final store = Store<AppState>(appReducer,
      initialState: AppState(),
      middleware: []
        ..addAll(createStoreAuthMiddleware())
        ..addAll(createStorePersistenceMiddleware())
        // STARTER: middleware - do not remove comment
        ..addAll([
          LoggingMiddleware.printer(),
        ]));

  runApp(LocalizedApp(delegate, SampleReduxApp(store: store)));
}

class SampleReduxApp extends StatefulWidget {
  final Store<AppState> store;

  SampleReduxApp({Key key, this.store}) : super(key: key);

  @override
  _SampleReduxAppState createState() => _SampleReduxAppState();
}

class _SampleReduxAppState extends State<SampleReduxApp> {
  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;
    return LocalizationProvider(
        state: LocalizationProvider.of(context).state,
        child: StoreProvider<AppState>(
          store: widget.store,
          child: MaterialApp(
            title: 'My App',
            localizationsDelegates: [
              localizationDelegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            supportedLocales: localizationDelegate.supportedLocales,
            locale: localizationDelegate.currentLocale,
            routes: {
              InitScreen.route: (context) => InitScreen(),
              LoginScreen.route: (context) => LoginScreen(),
              HomeScreen.route: (context) => HomeScreen(),
              // STARTER: routes - do not remove comment
            },
          ),
        ));
  }
}
